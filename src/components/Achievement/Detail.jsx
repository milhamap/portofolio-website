import React from 'react';
import { useParams } from 'react-router-dom';
import data from '../../utils/data.json'
import '../../assets/styles/achievement-detail.css';

const Detail = () => {
    const { achievementId } = useParams(); // Mendapatkan parameter ID dari URL
    const achievement = data.achievements.find(ach => ach.id === achievementId);

    if (!achievement) {
        return <div>Achievement not found</div>;
    }

    return (
        <section className="achievement-detail py-5">
            <div className="container">
                <div className='title-achievement-detail'>
                    <h2>{achievement.title} {achievement.name} {achievement.year} - {achievement.type}</h2>
                    <img src={achievement.certivicate} alt='Certivicate' className='image-achievement-detail'/>
                    <div className='desc-achievement-detail mt-4'>{achievement.description}</div>
                </div>
            </div>
        </section>
    );
}

export default Detail;

import React from 'react';
import data from '../../utils/data.json'
import '../../assets/styles/achievement.css';
import { Link } from 'react-router-dom'

const Index = () => {
    const achievements = data.achievements;

    return (
        <>
            <section className="achievement py-5">
                <div className='container'>
                    {achievements.map((achievement) => (
                        <Link to={`/achievement/${achievement.id}`} className="achieve-docs" key={achievement.id}>
                            <div className="achieve-content my-5 p-3">
                                <div className="row">
                                    <div className="col-lg-3 col-5">
                                        <img src={achievement.icon_light} alt="" className='achieve-icon'/>
                                    </div>
                                    <div className="col-lg-9 col-7">
                                        <h1>{achievement.title}</h1>
                                        <h2>{achievement.name} {achievement.year} - {achievement.type}</h2>
                                        <p>{achievement.abstract}</p>
                                    </div>
                                </div>
                            </div>
                        </Link>
                    ))}                        
                </div>
            </section>
        </>
    )
}

export default Index;

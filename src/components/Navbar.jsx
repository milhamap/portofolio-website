import { FontAwesomeIcon  } from '@fortawesome/react-fontawesome'
import { faLinkedin, faGithub, faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg sticky-top navbar-dark">
            <div className="container d-flex justify-content-between align-items-center">
                <div className="navbar-brand">ilham<span>.dev</span></div>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-center" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link className="nav-link text-capitalize" to={`/`} aria-current="page">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link text-capitalize" to={`/achievements`}>Achievement</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link text-capitalize" to={`/projects`}>Project</Link>
                        </li>
                    </ul>
                </div>
                <div className="navbar-icons d-none d-lg-block">
                    <a href="https://www.linkedin.com/in/milhamap/" target='__blank'>
                        <FontAwesomeIcon icon={faLinkedin} className="mx-2" />
                    </a>
                    <a href="mailto:ilhamap45@gmail.com">
                        <FontAwesomeIcon icon={faEnvelope} className="mx-2" />
                    </a>
                    <a href="https://github.com/milhamap" target='__blank'>
                        <FontAwesomeIcon icon={faGithub} className="mx-2" />
                    </a>
                    <a href="https://www.instagram.com/milhamapratama/" target='__blank'>
                        <FontAwesomeIcon icon={faInstagram} className="mx-2" />
                    </a>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;

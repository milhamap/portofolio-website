import React from 'react';
import data from '../../utils/data.json'
import '../../assets/styles/achievement.css';
import { Link } from 'react-router-dom'

const Index = () => {
    const projects = data.projects;

    return (
        <>
            <section className="achievement py-5">
                <div className='container'>
                    <div className="experience-details-container my-4">
                        <div className="row">
                            {projects.map((project) => (
                                <div className="col-12 col-lg-4 my-2" key={project.id}>
                                    <div className="details-container color-container">
                                        <div className="article-container">
                                            <div className="project-img">
                                                <img
                                                    src={project.image}
                                                    alt={project.title}
                                                />
                                            </div>
                                        </div>
                                        <Link to={`/project/${project.id}`} className="project-title">{project.title}</Link>
                                        <p>{project.abstract}</p>
                                        <div className="btn-container">
                                            {project.url_code && (
                                                <button
                                                    className="btn btn-color-2 project-btn"
                                                    onClick={() => window.location.href = project.url_code}
                                                >
                                                    Github
                                                </button>
                                            )}
                                            {project.url_app && (
                                                <button
                                                    className="btn btn-color-2 project-btn"
                                                    onClick={() => window.location.href = project.url_app}
                                                >
                                                    Live Demo
                                                </button>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>                        
                </div>
            </section>
        </>
    )
}

export default Index;

import React from 'react';
import { useParams } from 'react-router-dom';
import data from '../../utils/data.json'
import '../../assets/styles/project-detail.css';

const Detail = () => {
    const { projectId } = useParams(); // Mendapatkan parameter ID dari URL
    const project = data.projects.find(ach => ach.id === projectId);

    if (!project) {
        return <div>Project not found</div>;
    }

    return (
        <section className="project-detail py-5">
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-12">
                        <img src={project.image} alt='Certivicate' className='image-project-detail'/>
                    </div>
                    <div className="col-lg-8 col-12">
                        <div className='title-project-detail'>
                            <h2>{project.title}</h2>
                            <div className='desc-project-detail mt-4'>"{project.abstract}"</div>
                            <div className='desc-project-detail mt-4' dangerouslySetInnerHTML={{ __html: project.description.replace(/<br>/g, '<br/>')}} />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Detail;

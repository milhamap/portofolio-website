import { Link } from 'react-router-dom';

const Footer = () => {
    return (
        <footer className="py-5">
            <div class="container">
                <nav>
                    <div class="nav-links-container">
                        <ul class="nav-links">
                            <li><Link to={`/`}>Home</Link></li>
                            <li><Link to={`/achievements`}>Achievement</Link></li>
                            <li><Link to={`/projects`}>Projects</Link></li>
                        </ul>
                    </div>
                </nav>
                <p>Copyright &#169; 2023 Muhammad Ilham Adi Pratama. All Rights Reserved.</p>
            </div>
        </footer>
    )
}

export default Footer
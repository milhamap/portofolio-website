import { useEffect } from 'react';
import '../../assets/styles/home/wrapper.css'
import CV from '../../assets/pdf/CV.pdf'
import splash from '../../../public/splash.png'
import LogoMobile from '../../../public/Logo-Mobile.png'
import PhotoMe from '../../../public/Photo-Me.png'
import Typed from 'typed.js';
import { FontAwesomeIcon  } from '@fortawesome/react-fontawesome'
import {faLinkedin, faGithub, faInstagram} from '@fortawesome/free-brands-svg-icons'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

const Wrapper = () => {
    useEffect(() => {
        new Typed('.multiText', {
            strings: ["Ilham", "Backend", "Freelance"],
            loop: true,
            typeSpeed: 100,
            backSpeed: 100,
            backDelay: 1500,
        });
        new Typed('.multiTextMobile', {
            strings: ["M Ilham Adi Pratama", "Backend Developer", "Freelancer"],
            loop: true,
            typeSpeed: 100,
            backSpeed: 100,
            backDelay: 1500,
        });
    }, []);

    return (
        <section className="wrapper py-4">
            <div className="container">
                <div className="website">
                    <div className="row">
                        <div className="col-6 cols bg-transparent">
                            <span className="topline bg-transparent">Hello</span>
                            <h1 className="titleText bg-transparent">I'm <span className="multiText"></span></h1>
                            <p className="subTitleText bg-transparent">I'm interested in information technology and data science such as (Machine Learning, Backend Development, and Agile Development)</p>
                            <div className="buttonWrapper bg-transparent">
                                <a className="btn-cv text-decoration-none" href={CV} download={CV}>Download CV</a>
                                {/*<a className="btn-cv text-decoration-none">Download CV</a>*/}
                                <a href='#contact' className="btn-hire text-decoration-none">Hire Me</a>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="imageBox">
                                <img src={splash} alt="" id="splash"/>
                                <img src={PhotoMe} id="people" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mobile">
                    <div className="mobileImage">
                        <img src={LogoMobile} alt="" />
                    </div>
                    <div className="mobileContent mt-2">
                        <span className="topline">Hello</span>
                        <h1 className="titleText">I'm <span className="multiTextMobile"></span></h1>
                        <p className="subTitleText">I'm interested in information technology and data science such as (Machine Learning, Backend Development, and Agile Development)</p>
                        <div className="buttonWrapper bg-transparent">
                            <a className="btn-cv text-decoration-none" href={CV} download={CV}>Download CV</a>
                            {/*<a className="btn-cv text-decoration-none">Download CV</a>*/}
                            <a href='#contact' className="btn-hire text-decoration-none">Hire Me</a>
                        </div>
                        <div className="mobile-icons mt-2">
                            <a href="https://www.linkedin.com/in/milhamap/">
                                <FontAwesomeIcon icon={faLinkedin} className="mx-2" />
                            </a>
                            <a href="mailto:ilhamap45@gmail.com">
                                <FontAwesomeIcon icon={faEnvelope} className="mx-2" />
                            </a>
                            <a href="https://github.com/milhamap">
                                <FontAwesomeIcon icon={faGithub} className="mx-2" />
                            </a>
                            <a href="https://www.instagram.com/milhamapratama/" target='__blank'>
                                <FontAwesomeIcon icon={faInstagram} className="mx-2" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Wrapper;
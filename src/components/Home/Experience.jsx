import '../../assets/styles/home/experience.css';
import checkmark from '../../../public/checkmark.png';

const Experience = () => {
    return (
        <section className="experience py-5">
            <div className="container">
                <p className="section__text__p1">Explore My</p>
                <h1 className="title">Experience</h1>
                <div className="experience-details-container">
                    <div className="row">
                        <div className="col-12 col-lg-6 my-2">
                            <div className="details-container">
                                <h2 className="experience-sub-title">Backend Development</h2>
                                <div className="row">
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>SQL</h3>
                                        <p>Intermediate</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>JavaScript</h3>
                                        <p>Experienced</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>TypeScript</h3>
                                        <p>Intermediate</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>NodeJS</h3>
                                        <p>Experienced</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>ExpressJS</h3>
                                        <p>Experienced</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>NestJS</h3>
                                        <p>Basic</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>Laravel</h3>
                                        <p>Experienced</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>Git</h3>
                                        <p>Intermediate</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>Agile</h3>
                                        <p>Intermediate</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>DesignPattern</h3>
                                        <p>Basic</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                            src={checkmark}
                                            alt="Experience icon"
                                            className="icon"
                                        />
                                        <div>
                                            <h3>Golang</h3>
                                            <p>Basic</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-6 my-2">
                            <div className="details-container">
                                <h2 className="experience-sub-title">Frontend Development</h2>
                                <div className="row">
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>HTML</h3>
                                        <p>Experienced</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>CSS</h3>
                                        <p>Intermediate</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>SASS</h3>
                                        <p>Basic</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>VueJS</h3>
                                        <p>Intermediate</p>
                                        </div>
                                    </div>
                                    <div className='article col-6 col-md-4 my-2'>
                                        <img
                                        src={checkmark}
                                        alt="Experience icon"
                                        className="icon"
                                        />
                                        <div>
                                        <h3>ReactJS</h3>
                                        <p>Intermediate</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
      );
}

export default Experience;
import React from 'react';
import '../../assets/styles/home/achievement.css';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import data from '../../utils/data.json'
import { Link } from 'react-router-dom'

const Achievement = () => {
    const responsiveOptions = {
        0: {
          items: 1, // Jumlah item pada layar <= 0px
        },
        768: {
          items: 3, // Jumlah item pada layar > 768px
        },
    };

    const achievements = data.achievements;

    return (
        <section className="achievement py-5">
            <div className='container'>
                <p className="section__text__p1">Look At All My</p>
                <h1 className="title">Achievements</h1>
                <OwlCarousel
                    className="owl-theme py-5"
                    loop
                    margin={30}
                    responsive={responsiveOptions}
                    autoplay
                >
                    {achievements.map((achievement) => (
                        <Link to={`/achievement/${achievement.id}`} key={achievement.id} className="item">
                            <div className="achievement-carousel">
                                <div className="achievement-image-dark">
                                    <img src={achievement.icon_dark} alt=""/>
                                </div>
                                <div className="achievement-image-light">
                                    <img src={achievement.icon_light} alt=""/>
                                </div>
                                <h1>{achievement.title}</h1>
                                <h2>{achievement.name} {achievement.year} - {achievement.type}</h2>
                                <p>{achievement.abstract}</p>
                            </div>
                        </Link>
                    ))}
                </OwlCarousel>
            </div>
        </section>
    );
}

export default Achievement;
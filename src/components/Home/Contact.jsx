import { FontAwesomeIcon  } from '@fortawesome/react-fontawesome'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import '../../assets/styles/home/contact.css'

const Contact = () => {
    return (
        <section className="contact py-5" id='contact'>
            {/* <div className="container"> */}
                <p className="section__text__p1">Get in Touch</p>
                <h1 className="title">Contact Me</h1>
                <div className="contact-info-upper-container">
                    <div className="contact-info-container">
                        <FontAwesomeIcon icon={faEnvelope} className="contact-icon email-contact" />
                        <p><a href="mailto:ilhamap45@gmail.com">ilhamap45@gmail.com</a></p>
                    </div>
                    <div className="contact-info-container">
                        <FontAwesomeIcon icon={faLinkedin} className="contact-icon" />
                        <p><a href="https://www.linkedin.com/in/milhamap/" target='__blank'>LinkedIn</a></p>
                    </div>
                </div>
            {/* </div> */}
        </section>
    )
}

export default Contact
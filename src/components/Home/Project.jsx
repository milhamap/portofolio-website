import '../../assets/styles/home/project.css';
import data from '../../utils/data.json'
import { Link } from 'react-router-dom'
// import Bisnis from '../../assets/images/bisnis.png';

const projects = data.projects;

const Project = () => {

    return (
        <section className="project py-5">
            <div className='container'>
                <Link to={`/projects`} className="section__text__a d-none d-md-block">Lihat Selengkapnya</Link>
                <div>
                    <p className="section__text__p1">Browse My Recent</p>
                    <h1 className="title">Projects</h1>
                </div>
                <div className="experience-details-container my-4">
                    <div className="row">
                        {projects.map((project) => (
                            <div className="col-12 col-lg-4 my-2" key={project.id}>
                                <div className="details-container color-container">
                                    <div className="article-container">
                                        <div className="project-img">
                                            <img
                                                src={project.image}
                                                alt={project.title}
                                            />
                                        </div>
                                    </div>
                                    <Link to={`/project/${project.id}`} className="project-title">{project.title}</Link>
                                    <p>{project.abstract}</p>
                                    <div className="btn-container">
                                        {project.url_code && (
                                            <button
                                                className="btn btn-color-2 project-btn"
                                                onClick={() => window.open(project.url_code, '_blank')}
                                            >
                                                Github
                                            </button>
                                        )}
                                        {project.url_app && (
                                            <button
                                                className="btn btn-color-2 project-btn"
                                                onClick={() => window.open(project.url_app, '_blank')}
                                            >
                                                Live Demo
                                            </button>
                                        )}
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Project;
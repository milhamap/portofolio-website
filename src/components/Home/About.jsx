import '../../assets/styles/home/about.css'
import { FontAwesomeIcon  } from '@fortawesome/react-fontawesome'
import { faMedal, faCode, faAward, faUserGraduate } from '@fortawesome/free-solid-svg-icons'
import BgScroll from '../../../public/bg-scroll.jpg'

const About = () => {
    const aboutBg = {
        backgroundImage: `url(${BgScroll})`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundAttachment: 'fixed',
        color: '#FFE6C7',
    }
    return (
        <section className="about py-5" style={aboutBg}>
            <div className="container">
                <div className="row d-flex justify-content-center">
                    <div className="col-12 col-lg-3">
                    <div className='about-details-container'>
                            <div className="about-containers">
                                <div className="detail-container">
                                    <FontAwesomeIcon icon={faMedal} className="icon" />
                                    <h3>Achievement <br />10</h3>
                                    <p>Bussiness Idea and Web Application</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-lg-3">
                        <div className='about-details-container'>
                            <div className="about-containers">
                                <div className="detail-container">
                                    <FontAwesomeIcon icon={faCode} className="icon" />
                                    <h3>Project <br />8</h3>
                                    <p>Backend and Frontend</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-lg-3">
                        <div className='about-details-container'>
                            <div className="about-containers">
                                <div className="detail-container">
                                    <FontAwesomeIcon icon={faAward} className="icon" />
                                    <h3>Experience <br />2+ years</h3>
                                    <p>Backend Development</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-lg-3">
                        <div className='about-details-container'>
                            <div className="about-containers">
                                <div className="detail-container">
                                    <FontAwesomeIcon icon={faUserGraduate} className="icon" />
                                    <h3>Education</h3>
                                    <p>Undergraduate of Informatics Engineering at PENS</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default About;
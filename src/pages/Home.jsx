import Navbar from "../components/Navbar"
import Wrapper from "../components/Home/Wrapper"
import About from '../components/Home/About'
import Project from "../components/Home/Project"
import Experience from "../components/Home/Experience"
import Achievement from "../components/Home/Achievement"
import Contact from "../components/Home/Contact"
import Footer from "../components/Footer"

const Home = () => {
    return (
        <>
            <Navbar />
            <Wrapper />
            <Experience />
            <Project />
            <About />
            <Achievement />
            <Contact />
            <Footer />
        </>
    )
}

export default Home
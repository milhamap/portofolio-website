import Navbar from "../components/Navbar"
import Footer from "../components/Footer"
import Detail from "../components/Achievement/Detail"

const AchievementDetail = () => {
    return (
        <>
            <Navbar />
            <Detail />
            <Footer />
        </>
    )
}

export default AchievementDetail
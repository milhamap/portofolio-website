import Navbar from "../components/Navbar"
import Footer from "../components/Footer"
import Index from "../components/Project/Index"

const Achievement = () => {
    return (
        <>
            <Navbar />
            <Index />
            <Footer />
        </>
    )
}

export default Achievement
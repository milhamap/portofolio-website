import Navbar from "../components/Navbar"
import Footer from "../components/Footer"
import Detail from "../components/Project/Detail"

const ProjectDetail = () => {
    return (
        <>
            <Navbar />
            <Detail />
            <Footer />
        </>
    )
}

export default ProjectDetail
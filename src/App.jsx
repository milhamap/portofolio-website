import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Home from './pages/Home'
import Achievement from './pages/Achievement'
import AchievementDetail from './pages/AchievementDetail'
import Project from './pages/Project'
import ProjectDetail from './pages/ProjectDetail'

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/achievements" element={<Achievement />} />
        <Route path="/achievement/:achievementId" element={<AchievementDetail />} />
        <Route path="/projects" element={<Project />} />
        <Route path='/project/:projectId' element={<ProjectDetail />} />
      </Routes>
    </Router>
  )  
}

export default App
